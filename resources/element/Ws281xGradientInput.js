import {ColorInput, ColorInputTag} from "@framework/script/element/form/ColorInput.js";
import {SmartFormElement} from "@framework/script/element/form/SmartFormElement.js";
import {SmartButtonTag} from "@framework/script/element/SmartButton.js";

export class Ws281xGradientInput extends SmartFormElement {
    /**
     * @type {ColorInput[]}
     */
    colors = [];

    /**
     * @type {HTMLDivElement}
     */
    colorsWrapper = document.createElement("div");

    get value() {
        return this.colors.map((input) => input.value)
    }

    getContent() {
        const addColorButton = SmartButtonTag.small`Add Color`;
        addColorButton.addEventListener('click', this._addColor.bind(this));

        return [
            this.colorsWrapper,
            addColorButton,
        ];
    }

    _addColor() {
        const colorInput = new ColorInputTag(`gradient-color-part-${this.colors.length}`)`Gradient color`;

        this.colors.push(colorInput);
        this.colorsWrapper.appendChild(colorInput);
    }
}

const TagFunction = Ws281xGradientInput.register();

/**
 * @param {string} name
 * @returns {tagFunctionCallback<ColorInput>}
 */
export function Ws281xGradientInputTag(name) {
    return function Ws281xGradientInputTagInner(...content) {
        const element = TagFunction(...content);

        element.name = name;

        return element;
    };
}
