import {ApplicationWindow} from "@framework/script/element/ApplicationWindow.js";
import {DeviceActionButtonTag} from "@framework/script/element/DeviceActionButton.js";
import {SmartTitleTag} from "@framework/script/element/SmartTitle.js";

export class Ws281xActionsView extends ApplicationWindow {
    async getWindowComponents() {
        return [
            ...this._createSection(
                'Fill',
                [
                    this._getMethodAction('Color', 'fill_color_modal'),
                    this._getMethodAction('Gradient', 'fill_gradient_modal'),
                ],
            ),
            ...this._createSection(
                'Functions',
                [
                    this._getMethodAction('Rainbow', 'function_rainbow_modal'),
                    this._getMethodAction('Fade Color', 'function_fade_modal'),
                ],
            ),
        ];
    }

    /**
     * @param {string} text
     * @param {string} action
     * @returns {HTMLElement}
     * @private
     */
    _getMethodAction(text, action) {
        return DeviceActionButtonTag(this.application, this.channel, action)`${text}`;
    }

    /**
     * @param {string} title
     * @param {HTMLElement[]} actions
     * @returns {HTMLElement[]}
     * @private
     */
    _createSection(title, actions) {
        const smartTitle = SmartTitleTag.center`${title}`;
        smartTitle.classList.add('grid-container--full-width');

        return [smartTitle, ...actions];
    }
}

export const Ws281xActionsViewTag = Ws281xActionsView.register();
