import {ApplicationWindow} from '@framework/script/element/ApplicationWindow.js';
import {NumberInputTag} from "@framework/script/element/form/NumberInput.js";
import {SelectInputTag} from '@framework/script/element/form/SelectInput.js';
import {SmartFormTag} from '@framework/script/element/form/SmartForm.js';
import {SmartTextTag} from "@framework/script/element/SmartText.js";
import {SmartTitleTag} from '@framework/script/element/SmartTitle.js';

export class Ws281xSettingsView extends ApplicationWindow {
    output = document.createElement('span');

    /**
     * @returns {Promise<[HTMLElement]>}
     */
    async getWindowComponents() {
        const titleLine = SmartTitleTag`GPIO Channel Generator`;
        titleLine.classList.add('title-line');
        titleLine.classList.add('grid-container--full-width');

        const form = SmartFormTag``;
        form.classList.add('grid-container--full-width');
        form.addInput(SelectInputTag('scheme', ['ws2812', 'ws2811', 'sk6812rgb', 'sk6812rgbw'])`Strip Type`);
        form.addInput(SelectInputTag('pin', this._getPins())`GPIO Pin`);
        form.addInput(NumberInputTag('numLeds', {min: 0, max: 1024})`LED Count`);

        form.addEventListener('SmartForm.update', this._onUpdateInputs.bind(this));
        this._onUpdateInputs({detail: {getData: form.getData.bind(form)}});

        return [titleLine, form, SmartTextTag`Channel: ${this.output}`];
    }

    /**
     * @returns {[number]}
     *
     * @private
     */
    _getPins() {
        return [18, 12, 13];
    }

    /**
     * @param detail
     *
     * @private
     */
    _onUpdateInputs({detail}) {
        const {scheme, pin, numLeds} = detail.getData();
        this.output.innerText = `${scheme || 'ws2812'}://${pin || '0'}?numLeds=${numLeds}`;
    }
}

export const Ws281xSettingsViewTag = Ws281xSettingsView.register();
