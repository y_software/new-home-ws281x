import {Ws281xGradientInputTag} from "@app/element/Ws281xGradientInput.js";
import {ApplicationActionModalContent} from "@framework/script/element/ApplicationActionModalContent.js";

export class Ws281xFillGradientModal extends ApplicationActionModalContent {
    submitAction = 'fill_gradient';

    getInputs() {
        return [Ws281xGradientInputTag('gradient')``];
    }
}

export const Ws281xFillGradientModalTag = Ws281xFillGradientModal.register();
