import {ApplicationActionModalContent} from "@framework/script/element/ApplicationActionModalContent.js";
import {ColorInputTag} from "@framework/script/element/form/ColorInput.js";

export class Ws281xFillColorModal extends ApplicationActionModalContent {
    submitAction = 'fill_color';

    getInputs() {
        return [ColorInputTag('color')`Color`];
    }
}

export const Ws281xFillColorModalTag = Ws281xFillColorModal.register();
