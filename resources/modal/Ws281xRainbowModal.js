import {ApplicationActionModalContent} from "@framework/script/element/ApplicationActionModalContent.js";
import {NumberInputTag} from "@framework/script/element/form/NumberInput.js";

export class Ws281xFunctionRainbowModal extends ApplicationActionModalContent {
    submitAction = 'function_rainbow';

    getInputs() {
        return [
            NumberInputTag('speed', {value: 1})`Rainbow Speed`,
            NumberInputTag('offset', {value: 1})`Gradient Stretch`
        ];
    }
}

export const Ws281xFunctionRainbowModalTag = Ws281xFunctionRainbowModal.register();
