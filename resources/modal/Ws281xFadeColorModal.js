import {ApplicationActionModalContent} from "@framework/script/element/ApplicationActionModalContent.js";
import {ColorInputTag} from "@framework/script/element/form/ColorInput.js";
import {NumberInputTag} from "@framework/script/element/form/NumberInput.js";

export class Ws281xFadeColorModal extends ApplicationActionModalContent {
    submitAction = 'function_fade';

    getInputs() {
        return [
            ColorInputTag('color')`Color`,
            NumberInputTag('fade_duration', {value: 120})`Fade Duration`
        ];
    }
}

export const Ws281xFadeColorModalTag = Ws281xFadeColorModal.register();
