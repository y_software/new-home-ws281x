const path = require('path');

module.exports = {
    resolve: {
        alias: {
            '@app': path.resolve(__dirname, 'resources'),
            '@framework': path.resolve(__dirname, '../../js/new-home-ui/public'),
        }
    }
}
