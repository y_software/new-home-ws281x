[![Pipeline Status](https://gitlab.com/Y_Software/new-home-ws281x/badges/master/pipeline.svg)](https://gitlab.com/Y_Software/new-home-ws281x/pipelines)
[![creates.io](https://img.shields.io/crates/v/new-home-ws281x.svg?style=flat-square&logo=rust)](https://crates.io/crates/new-home-ws281x)

# New Home WS 281x

This application will control all kinds of LED strips based on the ws281x chip.

## Features (Planed)

|Feature|Status|
|---|---|
|Static color|done|
|Gradient|done|
|LED Rainbow|done|
|Random, glowing, fading Lights|next|
|Pre-Programmed light sequence|next|

### Functional features

|Feature|Status|
|---|---|
|Gamma Correction|done|
|Shader-Like rendering|done|
|Fixed rendering TPS|done|
|Set bind-address and port from arguments/config file|next|
|Concurrent rendering|future|

## Supported Chips

- WS2811
- WS2812(b)
- SK6812RGB(W)
- More WS281X chips could be supported with the WS2812 and WS2811 engines but I cannot guarantee any compatibility

## Install and setup

### Install
To install the application you have to run the `(sudo) make install` command in the project folder as the root user.
This will install all necessary files where they are needed.

Configuration and resources of the application can be found in the `/etc/new-home-ws281x` directory.

### Setup

For the setup I assume, that you can, by yourself, connect and troubleshoot your LED Strip/Matrix to the Raspberry PI.

For this application to work you need some things:

- An already setup [new-home-core](https://gitlab.com/Y_Software/new-home-core)
- An already setup [new-home-ui](https://gitlab.com/Y_Software/new-home-ui)
- The IP of your Raspberry PI which runs the application

The default port on which the application runs is the 4232, and it listens an all IPv4 and IPv6 interfaces/addresses on the Raspberry PI.

Setup is as easy as for all applications. Add a new application in the Applications view in the UI, enter a name for the application, and the IP:PORT for the Raspberry PI.
Now you can add devices to rooms, which are running on the PI. If you need help with the required channel, you can take a look into the settings of the application, where you can find a channel builder.

### Uninstall

To uninstall this application you can just run `(sudo) make uninstall`. *This will remove the application and all its configuration permanently.*

### Channel builder

To use the channel builder, you just click on the registered application in the applications view.
There you will find 3 combo-boxes which show you the supported controller types, GPIO pins and RGB/BGR etc. mode, which you can perform on a pin.

The shown numbers represent the GPIO pins by their BCM number. For more information on which pin has which number, you can check out [https://pinout.xyz/](https://pinout.xyz/). 

## Footnote

Although the underlying framework ([new-home-application](https://gitlab.com/Y_Software/new-home-application) and [new-home-core](https://gitlab.com/Y_Software/new-home-core))
is not yet considered "stable" by the meaning of "api will not change" and "works without any crashes", this application, as it is, is considered stable under this term.
