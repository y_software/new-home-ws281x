.PHONY: default
.PHONY: install
.PHONY: uninstall

default:
	@echo "You have to specify install or uninstall."

install:
	cargo check --all
	cargo build --release
	install systemd/new-home-ws281x.service /usr/lib/systemd/system/new-home-ws281x.service
	install target/release/new-home-ws281x /usr/bin/new-home-ws281x
	mkdir -p /etc/new-home-ws281x/resources
	cp -R resources/* /etc/new-home-ws281x/resources

uninstall:
	rm /usr/lib/systemd/system/new-home-ws281x.service
	rm /usr/bin/new-home-ws281x
	rm -r /etc/new-home-ws281x
