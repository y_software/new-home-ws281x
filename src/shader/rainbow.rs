use palette::Hsl;

use crate::services::strip_shader::StripShader;
use crate::util::color_parser::ColorParser;

pub struct Rainbow {
    speed: f32,
    offset: f32,
}

impl Rainbow {
    pub fn new(speed: f32, offset: f32) -> Self {
        Rainbow { speed, offset }
    }
}

impl StripShader for Rainbow {
    fn render(&self, led: usize, _leds: usize, tick: u64) -> [u8; 4] {
        let parser = ColorParser::from(Hsl::new(
            ((tick as f32 * self.speed) + (led as f32 * self.offset)) % 360_f32,
            1.0,
            0.5,
        ));

        parser.ws_rgb()
    }
}
