use std::f32::consts::PI;

use crate::services::strip_shader::StripShader;
use crate::util::color_parser::ColorParser;

pub struct StaticFade {
    color: [u8; 4],
    fade_duration: f32,
}

impl StaticFade {
    pub fn new(color: [u8; 4], fade_duration: f32) -> Self {
        Self {
            color,
            fade_duration,
        }
    }
}

impl StripShader for StaticFade {
    fn render(&self, _led: usize, _leds: usize, tick: u64) -> [u8; 4] {
        let sin = ((tick as f32 % self.fade_duration) / self.fade_duration * PI).sin();
        let mut color = self.color.clone();

        color[0] = (sin * color[0] as f32) as u8;
        color[1] = (sin * color[1] as f32) as u8;
        color[2] = (sin * color[2] as f32) as u8;

        ColorParser::correct_gamma(color)
    }
}

unsafe impl Sync for StaticFade {}

unsafe impl Send for StaticFade {}
