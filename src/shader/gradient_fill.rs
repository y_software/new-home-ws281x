use palette::{Gradient, LinSrgb};

use crate::services::strip_shader::StripShader;
use crate::util::color_parser::ColorParser;

pub struct GradientFill {
    gradient: Gradient<LinSrgb>,
}

impl GradientFill {
    pub fn new(gradient_colors: Vec<LinSrgb>) -> Self {
        Self { gradient: Gradient::new(gradient_colors.clone()) }
    }
}

impl StripShader for GradientFill {
    fn render(&self, led: usize, leds: usize, _tick: u64) -> [u8; 4] {
        let color: LinSrgb = self.gradient.get(led as f32 / leds as f32);

        ColorParser::correct_gamma([
            (color.blue * 255_f32) as u8,
            (color.green * 255_f32) as u8,
            (color.red * 255_f32) as u8,
            0
        ])
    }
}

unsafe impl Sync for GradientFill {}

unsafe impl Send for GradientFill {}
