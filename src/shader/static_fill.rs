use crate::services::strip_shader::StripShader;

pub struct StaticFill {
    color: [u8; 4],
}

impl StaticFill {
    pub fn new(color: [u8; 4]) -> Self {
        Self { color }
    }
}

impl StripShader for StaticFill {
    fn render(&self, _led: usize, _leds: usize, _tick: u64) -> [u8; 4] {
        self.color.clone()
    }
}

unsafe impl Sync for StaticFill {}

unsafe impl Send for StaticFill {}
