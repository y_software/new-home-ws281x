use std::sync::{Arc, Mutex};
use std::sync::mpsc::Receiver;
use std::time::{Duration, SystemTime};

use rs_ws281x::Controller;

use crate::services::strip_shader::StripShader;

/// Renders a shader onto a strip/controller
pub struct StripRenderer {
    controller: Arc<Mutex<Controller>>,
    interrupt_receiver: Receiver<bool>,
    shader: Option<Box<dyn StripShader + Send + Sync>>,
    tps: u16,
    should_stop: bool,
    running_mtx: Mutex<i8>,
}

impl StripRenderer {
    pub fn new(controller: Arc<Mutex<Controller>>, interrupt_receiver: Receiver<bool>) -> Self {
        Self {
            controller,
            interrupt_receiver,
            shader: None,
            tps: 25,
            should_stop: true,
            running_mtx: Mutex::new(0),
        }
    }

    /// Sets the shader which should be rendered
    pub fn set_shader(&mut self, shader: Option<Box<dyn StripShader + Send + Sync>>) {
        self.shader = shader;
    }

    /// Starts the rendering loop
    pub fn start(&mut self) {
        self.should_stop = false;
        self.run_loop_thread();
    }

    /// Loops with a fixed rate of 25 TPS.
    /// Renders the set shader (if there is one) to the given controller
    /// Stops if it receives a "true" from the interrupt_receiver
    fn run_loop_thread(&self) {
        let mut tick: u64 = 0;
        let _lock = self.running_mtx.lock();

        loop {
            let start_time = SystemTime::now();

            self.call_shader(tick);

            let mut sleep_millis = (1000 / self.tps) as i64;
            let tick_time = start_time.elapsed().unwrap().as_millis();
            sleep_millis -= tick_time as i64;

            if sleep_millis < 0 {
                sleep_millis = 1;
                println!("[WARN]: Tick took to long! Took {}ms", tick_time);
            }

            tick += 1;

            let stop = match self.interrupt_receiver.recv_timeout(Duration::from_millis(sleep_millis as u64)) {
                Ok(value) => value,
                Err(_err) => false
            };

            if stop {
                break;
            }
        }

        drop(_lock);
    }

    /// Calls the set shader for each LED of the set controller
    /// Sends the number of LEDs, the number of the current LED and the tick into the shader for further processing
    fn call_shader(&self, tick: u64) {
        let mut controller = self.controller.lock().unwrap();
        let leds = controller.leds_mut(0);

        match &self.shader {
            Some(shader) => {
                let led_count = leds.len();
                for led in 0..led_count {
                    // [TODO][LOW]: Wait for Rust to have a stable async/await system and rewrite shader to be called concurrent
                    leds[led] = shader.render(led, led_count, tick);
                }

                match controller.render() {
                    Err(error) => {
                        println!("Could not render strip {:?}", error);
                    }
                    Ok(_) => {}
                }
            }
            None => {}
        };

        drop(controller);
    }
}

unsafe impl Send for StripRenderer {}

unsafe impl Sync for StripRenderer {}
