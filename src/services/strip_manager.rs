use std::collections::HashMap;
use std::sync::{Arc, Mutex, RwLock};

use rs_ws281x::{ChannelBuilder, Controller, ControllerBuilder, StripType};

use crate::util::strip_channel_translator::StripChannelTranslator;
use crate::util::ws281x_error::Ws281xError;

/// Keeps track of known LED Strip channels and already created controllers
pub struct StripManager {
    channel_controller: RwLock<HashMap<String, Arc<Mutex<Controller>>>>,
}

impl StripManager {
    pub fn new() -> Self {
        Self { channel_controller: Default::default() }
    }

    /// Gets an already created cntroller from the given channel or creates a new one
    pub fn get_strip_controller(&self, channel: String) -> Result<Arc<Mutex<Controller>>, Ws281xError> {
        let mut channel_controller = self.channel_controller.write().unwrap();

        return match { channel_controller.get(&channel) } {
            Some(controller) => Ok(controller.clone()),
            None => {
                let strip_channel = StripChannelTranslator::translate_channel(channel.clone())?;
                let controller = self.build_controller(strip_channel.gpio_pin, strip_channel.count, strip_channel.strip_type)?;

                channel_controller.insert(channel.clone(), controller.clone());

                Ok(controller.clone())
            }
        };
    }

    /// Builds the controller and channel for the given strip/channel info
    ///
    /// Outputs a Ws281xError if the ws281x library could not create the controller, outputs a precise log into the console
    fn build_controller(&self, gpionum: i32, count: i32, strip_type: StripType) -> Result<Arc<Mutex<Controller>>, Ws281xError> {
        let mut channel_0 = ChannelBuilder::new();
        channel_0.brightness(0xFF)
            .pin(gpionum)
            .count(count)
            .strip_type(strip_type);

        match ControllerBuilder::new()
            .channel(
                0,
                channel_0.build(),
            )
            .channel(1, ChannelBuilder::new().build())
            .build() {
            Ok(ctrl) => Ok(Arc::new(Mutex::new(ctrl))),
            Err(_error) => {
                #[cfg(debug_assertions)]
                println!("{:?}", _error);

                return Err(Ws281xError::new(String::from("Could not build strip controller!")));
            }
        }
    }
}
