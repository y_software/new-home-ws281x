use std::collections::HashMap;
use std::mem::replace;
use std::sync::{Arc, Mutex, RwLock};
use std::sync::mpsc::{channel, Sender};
use std::thread;
use std::thread::JoinHandle;

use crate::services::strip_manager::StripManager;
use crate::services::strip_renderer::StripRenderer;
use crate::services::strip_shader::StripShader;
use crate::util::ws281x_error::Ws281xError;

/// Contains all fields which are required for starting a new render process
struct RendererInfo {
    pub sender: Sender<bool>,
    pub renderer: Arc<Mutex<StripRenderer>>,
    pub join_handle: Option<JoinHandle<()>>,
}

type SafeRendererInfo = Arc<RwLock<RendererInfo>>;

/// Launches new shader processes for led strips
pub struct RenderManager {
    strip_manager: Arc<RwLock<StripManager>>,
    renderer: RwLock<HashMap<String, SafeRendererInfo>>,
}

impl RenderManager {
    pub fn new(strip_manager: Arc<RwLock<StripManager>>) -> Self {
        Self {
            strip_manager,
            renderer: RwLock::new(HashMap::new()),
        }
    }

    /// Sets the shader for the given channel and launches a new rendering thread
    pub fn set_shader_for(&self, channel: String, shader: Option<Box<dyn StripShader + Send + Sync>>) -> Result<(), Ws281xError> {
        let arc_info = self.get_renderer(&channel)?;
        let mut info = arc_info.write().unwrap();
        let handle_opt = replace(&mut info.join_handle, None);

        match handle_opt {
            Some(handle) => {
                info.sender.send(true).unwrap();
                handle.join().unwrap();
            }
            None => {}
        }

        let thread_renderer = info.renderer.clone();
        info.join_handle = Some(thread::spawn(move || {
            let mut renderer = thread_renderer.lock().unwrap();
            renderer.set_shader(shader);
            renderer.start();
        }));

        Ok(())
    }

    /// Gets a registered renderer info struct, or builds a new one.
    fn get_renderer(&self, channel: &String) -> Result<SafeRendererInfo, Ws281xError> {
        let mut renderer_rw = self.renderer.write().unwrap();
        let contains = { renderer_rw.contains_key(channel) };

        if !contains {
            renderer_rw.insert(
                channel.clone(),
                Arc::new(RwLock::new(self.build_renderer_info(channel)?)),
            );
        }

        Ok(renderer_rw.get(channel).unwrap().clone())
    }

    /// Builds the whole renderer info struct
    fn build_renderer_info(&self, channel: &String) -> Result<RendererInfo, Ws281xError> {
        let (renderer, sender) = self.build_renderer(channel)?;

        Ok(RendererInfo {
            sender,
            renderer: Arc::new(Mutex::new(renderer)),
            join_handle: None,
        })
    }

    /// Builds the Strip Renderer and a channel pair for communication
    ///
    /// Outputs an Ws281xError when the given controller for the channel could not be created.
    /// For more details in errors take a look into the `crate::services::strip_manager::StripManager` struct
    fn build_renderer(&self, strip_channel: &String) -> Result<(StripRenderer, Sender<bool>), Ws281xError> {
        let (sender, receiver) = channel::<bool>();
        let controller = self.strip_manager.read().unwrap().get_strip_controller(strip_channel.clone())?;

        Ok((StripRenderer::new(controller, receiver), sender))
    }
}
