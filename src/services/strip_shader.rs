/// Add the shader function to a struct
/// Should be able to run in parallel for each LED
pub trait StripShader {
    /// Returns the value for each sub-pixel as an u8 array in BGRW order
    ///
    /// # Arguments
    ///
    /// * `_led` the LED that is rendered in the current call
    /// * `_leds` the max count of LEDs on the strip
    /// * `_tick` the tick since the shader started running
    fn render(&self, _led: usize, _leds: usize, _tick: u64) -> [u8; 4];
}
