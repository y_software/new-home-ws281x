use std::error::Error;
use std::fmt::{Debug, Display, Formatter, Result};

pub struct Ws281xError {
    message: String,
}

impl Ws281xError {
    pub fn new(message: String) -> Self {
        Self { message }
    }
}

impl Error for Ws281xError {}

impl Debug for Ws281xError {
    fn fmt(&self, formatter: &mut Formatter<'_>) -> Result {
        write!(formatter, "{}", &self.message)
    }
}

impl Display for Ws281xError {
    fn fmt(&self, formatter: &mut Formatter<'_>) -> Result {
        write!(formatter, "{}", &self.message)
    }
}
