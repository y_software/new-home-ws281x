use palette::{Hsl, Hsv};
use palette::encoding::Srgb;
use palette::rgb::Rgb;

use crate::util::ws281x_error::Ws281xError;

const GAMMA: [u8; 256] = [
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1,
    1, 1, 1, 1, 1, 1, 1, 1, 1, 2, 2, 2, 2, 2, 2, 2,
    2, 3, 3, 3, 3, 3, 3, 3, 4, 4, 4, 4, 4, 5, 5, 5,
    5, 6, 6, 6, 6, 7, 7, 7, 7, 8, 8, 8, 9, 9, 9, 10,
    10, 10, 11, 11, 11, 12, 12, 13, 13, 13, 14, 14, 15, 15, 16, 16,
    17, 17, 18, 18, 19, 19, 20, 20, 21, 21, 22, 22, 23, 24, 24, 25,
    25, 26, 27, 27, 28, 29, 29, 30, 31, 32, 32, 33, 34, 35, 35, 36,
    37, 38, 39, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 50,
    51, 52, 54, 55, 56, 57, 58, 59, 60, 61, 62, 63, 64, 66, 67, 68,
    69, 70, 72, 73, 74, 75, 77, 78, 79, 81, 82, 83, 85, 86, 87, 89,
    90, 92, 93, 95, 96, 98, 99, 101, 102, 104, 105, 107, 109, 110, 112, 114,
    115, 117, 119, 120, 122, 124, 126, 127, 129, 131, 133, 135, 137, 138, 140, 142,
    144, 146, 148, 150, 152, 154, 156, 158, 160, 162, 164, 167, 169, 171, 173, 175,
    177, 180, 182, 184, 186, 189, 191, 193, 196, 198, 200, 203, 205, 208, 210, 213,
    215, 218, 220, 223, 225, 228, 231, 233, 236, 239, 241, 244, 247, 249, 252, 255
];

/// Converts between different color formatting between the palette and ws281x crates
pub struct ColorParser {
    color: Hsl
}

impl ColorParser {
    /// Builds itself from a given hsl string
    ///
    /// # Arguments
    ///
    /// `hsl_string` formatted like the CSS version as `hsl(hue as deg, saturation as %, lightness as %)`
    pub fn new(hsl_string: String) -> Result<Self, Ws281xError> {
        Ok(Self {
            color: Self::string_to_hsl(hsl_string)?
        })
    }

    /// Corrects the gamma for the given color in the ws281x format
    pub fn correct_gamma(color: [u8; 4]) -> [u8; 4] {
        let mut color_corrected = color.clone();

        color_corrected[0] = GAMMA[color_corrected[0] as usize];
        color_corrected[1] = GAMMA[color_corrected[1] as usize];
        color_corrected[2] = GAMMA[color_corrected[2] as usize];
        color_corrected[3] = GAMMA[color_corrected[3] as usize];

        color_corrected
    }

    /// Gets the rgb/bgr format which is used in the ws281x library
    pub fn ws_rgb(&self) -> [u8; 4] {
        let rgb = self.rgb();

        let color = [
            (rgb.blue * 255_f32) as u8,
            (rgb.green * 255_f32) as u8,
            (rgb.red * 255_f32) as u8,
            0
        ];

        Self::correct_gamma(color)
    }

    /// Creates the color as Rgb struct from the palette crate
    pub fn rgb(&self) -> Rgb {
        let rgb: Rgb = Rgb::from(self.hsl());

        rgb
    }

    /// Creates the color as Hsv struct from the palette crate
    pub fn hsv(&self) -> Hsv {
        let hsv: Hsv = Hsv::from(self.hsl());

        hsv
    }

    /// Creates the color as Hsl struct from the palette crate
    pub fn hsl(&self) -> Hsl {
        Hsl::from_components((self.color.hue, self.color.saturation, self.color.lightness))
    }

    /// Converts the given hsl string into the HSL struct from the palette crate
    ///
    /// Outputs an error if hue/saturation/lightness is not set or not correctly formatted
    fn string_to_hsl(hsl_string: String) -> Result<Hsl, Ws281xError> {
        let mut color = hsl_string.replace("hsl", "");
        color = String::from(color.trim_matches('(').trim_matches(')'));

        let color_tokens: Vec<&str> = color.split(",").collect();

        let hue = match color_tokens[0].parse::<f32>() {
            Ok(hue) => hue,
            Err(_err) => {
                #[cfg(debug_assertions)]
                println!("Hue parsing failed: {:?}", _err);

                return Err(Ws281xError::new(String::from("Could not parse hue")));
            }
        };

        let saturation = match color_tokens[1].trim_matches('%').trim_matches(' ').parse::<f32>() {
            Ok(saturation) => saturation / 100_f32,
            Err(_err) => {
                #[cfg(debug_assertions)]
                println!("Saturation parsing failed: {:?}", _err);

                return Err(Ws281xError::new(String::from("Could not parse saturation")));
            }
        };

        let lightness = match color_tokens[2].trim_matches('%').trim_matches(' ').parse::<f32>() {
            Ok(lightness) => lightness / 100_f32,
            Err(_err) => {
                #[cfg(debug_assertions)]
                println!("Lightness parsing failed: {:?}", _err);

                return Err(Ws281xError::new(String::from("Could not parse lightness")));
            }
        };

        Ok(Hsl::new(hue, saturation, lightness))
    }
}

/// Creates the ColorParser from an already existing Hsl from the palette crate
impl From<Hsl> for ColorParser {
    fn from(hsl: Hsl<Srgb, f32>) -> Self {
        Self {
            color: hsl
        }
    }
}
