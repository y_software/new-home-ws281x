use std::collections::HashMap;

use rs_ws281x::StripType;
use url::Url;

use crate::util::strip_type_mapper::StripTypeMapper;
use crate::util::ws281x_error::Ws281xError;

/// This struct contains the fields, which are contained inside the channel URL
pub struct StripChannel {
    pub strip_type: StripType,
    pub count: i32,
    pub gpio_pin: i32,
}

/// This struct translates the channel string into the appropriate struct with its values
pub struct StripChannelTranslator {}

impl StripChannelTranslator {
    /// Translates the URL/String channel into the struct with its fields
    ///
    /// Outputs the Ws281xError when
    /// - The Channel URL is invalid
    /// - Any of the above fields is not set or has a wrong type
    pub fn translate_channel(channel: String) -> Result<StripChannel, Ws281xError> {
        let parsed = match Url::parse(channel.as_str()) {
            Err(_error) => {
                #[cfg(debug_assertions)]
                println!("{:?}", _error);

                return Err(Ws281xError::new(String::from("Could not parse channel URL!")));
            }
            Ok(parsed) => parsed
        };

        let strip_type = StripTypeMapper::map_type(String::from(parsed.scheme()))?;

        let gpio_pin = match parsed.host() {
            Some(host) => match host.to_string().parse::<i32>() {
                Err(_error) => {
                    #[cfg(debug_assertions)]
                    println!("{:?}", _error);

                    return Err(Ws281xError::new(String::from("Gpio pin from URL is not a number!")));
                }
                Ok(pin) => pin
            },

            None => {
                return Err(Ws281xError::new(String::from("Gpio pin is not set in the channel URL!")));
            }
        };

        let query_params: HashMap<String, String> = parsed.query_pairs().into_owned().collect();

        let count = match query_params.get("numLeds") {
            Some(num_leds) => {
                match num_leds.parse::<i32>() {
                    Err(_error) => {
                        #[cfg(debug_assertions)]
                        println!("{:?}", _error);

                        return Err(Ws281xError::new(String::from("LED count from URL is not a number!")));
                    }
                    Ok(count) => count
                }
            }
            None => {
                return Err(Ws281xError::new(String::from("Num Leds parameter is not set!")));
            }
        };

        Ok(StripChannel {
            strip_type,
            gpio_pin,
            count,
        })
    }
}
