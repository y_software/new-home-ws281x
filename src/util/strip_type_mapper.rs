use rs_ws281x::StripType;

use crate::util::ws281x_error::Ws281xError;

pub struct StripTypeMapper {}

impl StripTypeMapper {
    pub fn map_type(strip_type_name: String) -> Result<StripType, Ws281xError> {
        match strip_type_name.as_str() {
            "ws2811" => Ok(StripType::Ws2811Rgb),
            "ws2812" => Ok(StripType::Ws2812),
            "sk6812rgb" => Ok(StripType::Sk6812),
            "sk6812rgbw" => Ok(StripType::Sk6812Rgbw),
            _ => Err(Ws281xError::new(String::from("Strip type is not supported!")))
        }
    }
}
