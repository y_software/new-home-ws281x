extern crate new_home_application;
#[macro_use]
extern crate serde_json;
extern crate url;

use std::sync::{Arc, RwLock};

use new_home_application::application::app_error::AppError;
use new_home_application::application::application::ApplicationInfo;
use new_home_application::application::application_framework::{
    Application, ApplicationFramework, BootApplication,
};
use new_home_application::method::method_manager::MethodManager;
use new_home_application::method::method_structure::{Method, MethodArgument};

use crate::methods::fill_color::FillColor;
use crate::methods::fill_gradient::FillGradient;
use crate::methods::function_rainbow::FunctionRainbow;
use crate::methods::get_script::GetScript;
use crate::methods::util::action_message::ActionMessage;
use crate::services::render_manager::RenderManager;
use crate::services::strip_manager::StripManager;
use crate::methods::function_fade::FunctionFade;

pub mod methods;
pub mod services;
pub mod shader;
pub mod util;

struct Ws281xApplication {}

impl Ws281xApplication {
    pub fn new() -> Self {
        Self {}
    }
}

impl ApplicationFramework for Ws281xApplication {
    fn get_application_port(&self) -> i16 {
        4232
    }

    fn get_application_ip(&self) -> String {
        String::from("[::]")
    }

    fn get_application_info(&self) -> ApplicationInfo {
        ApplicationInfo {
            name: "ws281x-application".to_string(),
            description: "Controls WS2812, WS2811 and SK6812RGB(W) LED strips".to_string(),
            authors: "Yannik_Sc".to_string(),
            version: "1.0.4".to_string(),
        }
    }

    fn setup_method_manager(&self, method_manager: &mut Box<dyn MethodManager>) {
        let strip_manager = Arc::new(RwLock::new(StripManager::new()));
        let render_manager = Arc::new(RwLock::new(RenderManager::new(strip_manager)));

        method_manager.add_method(
            Method {
                name: "device_action".to_string(),
                description: "Loads an actions view for the LED strip".to_string(),
                help: String::new(),
                arguments: vec![],
            },
            Box::new(ActionMessage::new(json!({
                "component": "ws281x-actions-view",
                "scripts": ["@app/view/Ws281xActionsView.js"]
            }))),
        );

        method_manager.add_method(
            Method {
                name: "fill_color".to_string(),
                description: "Fills an LED strip with the given color".to_string(),
                help: String::new(),
                arguments: vec![
                    MethodArgument {
                        name: "color".to_string(),
                        description: "The color for the LED Strip in HSL format".to_string(),
                        help: "".to_string(),
                        value: None,
                        required: true,
                    },
                    MethodArgument {
                        name: "channel".to_string(),
                        description: "The information how to control the LED strip formatted as URL".to_string(),
                        help: "".to_string(),
                        value: None,
                        required: true,
                    }
                ],
            },
            Box::new(FillColor::new(render_manager.clone())),
        );

        method_manager.add_method(
            Method {
                name: "fill_gradient".to_string(),
                description: "Fills an LED strip with the given gradient colors".to_string(),
                help: String::new(),
                arguments: vec![
                    MethodArgument {
                        name: "gradient".to_string(),
                        description: "The colors for the LED Strip in HSL format".to_string(),
                        help: "".to_string(),
                        value: None,
                        required: true,
                    },
                    MethodArgument {
                        name: "channel".to_string(),
                        description: "The information how to control the LED strip formatted as URL".to_string(),
                        help: "".to_string(),
                        value: None,
                        required: true,
                    }
                ],
            },
            Box::new(FillGradient::new(render_manager.clone())),
        );

        method_manager.add_method(
            Method {
                name: "get_script".to_string(),
                description: "Gets a script file from the resources directory".to_string(),
                help: String::new(),
                arguments: vec![MethodArgument {
                    name: "script_name".to_string(),
                    description: "The path to the script, that should be provided".to_string(),
                    help: String::new(),
                    value: None,
                    required: true,
                }],
            },
            Box::new(GetScript::new()),
        );

        method_manager.add_method(
            Method {
                name: "config_root".to_string(),
                description: "Gets a config view, which contains a channel builder".to_string(),
                help: String::new(),
                arguments: vec![],
            },
            Box::new(ActionMessage::new(json!({
                "component": "ws281x-settings-view",
                "scripts": ["@app/view/Ws281xSettingsView.js"]
            }))),
        );

        method_manager.add_method(
            Method {
                name: "fill_color_modal".to_string(),
                description: "Gets the action message for the color modal".to_string(),
                help: String::new(),
                arguments: vec![],
            },
            Box::new(ActionMessage::new(json!({
                "modal": {
                    "title": "Fill Strip",
                    "component": "ws281x-fill-color-modal",
                    "close": "Cancel"
                },
                "scripts": ["@app/modal/Ws281xFillColorModal.js"]
            }))),
        );

        method_manager.add_method(
            Method {
                name: "fill_gradient_modal".to_string(),
                description: "Gets the action message for the gradient modal".to_string(),
                help: String::new(),
                arguments: vec![],
            },
            Box::new(ActionMessage::new(json!({
                "modal": {
                    "title": "Fill Strip Gradient",
                    "component": "ws281x-fill-gradient-modal",
                    "close": "Cancel"
                },
                "scripts": ["@app/modal/Ws281xFillGradientModal.js"]
            }))),
        );

        method_manager.add_method(
            Method {
                name: "function_rainbow_modal".to_string(),
                description: "Gets the action message for the gradient modal".to_string(),
                help: String::new(),
                arguments: vec![],
            },
            Box::new(ActionMessage::new(json!({
                "modal": {
                    "title": "Rainbow",
                    "component": "ws281x-function-rainbow-modal",
                    "close": "Cancel"
                },
                "scripts": ["@app/modal/Ws281xRainbowModal.js"]
            }))),
        );

        method_manager.add_method(
            Method {
                name: "function_rainbow".to_string(),
                description: "Starts a rainbow effect on the given strip".to_string(),
                help: String::new(),
                arguments: vec![
                    MethodArgument {
                        name: "channel".to_string(),
                        description: "The information how to control the LED strip formatted as URL".to_string(),
                        help: "".to_string(),
                        value: None,
                        required: true,
                    },
                    MethodArgument {
                        name: "offset".to_string(),
                        description: "Defines the distance between the LEDs in the gradient".to_string(),
                        help: "".to_string(),
                        value: None,
                        required: true,
                    },
                    MethodArgument {
                        name: "speed".to_string(),
                        description: "Defines the speed of the Rainbow".to_string(),
                        help: "".to_string(),
                        value: None,
                        required: true,
                    }
                ],
            },
            Box::new(FunctionRainbow::new(render_manager.clone())),
        );

        method_manager.add_method(
            Method {
                name: "function_fade_modal".to_string(),
                description: "Gets the action message for the fade color modal".to_string(),
                help: String::new(),
                arguments: vec![],
            },
            Box::new(ActionMessage::new(json!({
                "modal": {
                    "title": "Rainbow",
                    "component": "ws281x-fade-color-modal",
                    "close": "Cancel"
                },
                "scripts": ["@app/modal/Ws281xFadeColorModal.js"]
            }))),
        );

        method_manager.add_method(
            Method {
                name: "function_fade".to_string(),
                description: "Starts the fade effect with the given color".to_string(),
                help: String::new(),
                arguments: vec![
                    MethodArgument {
                        name: "channel".to_string(),
                        description: "The information how to control the LED strip formatted as URL".to_string(),
                        help: "".to_string(),
                        value: None,
                        required: true,
                    },
                    MethodArgument {
                        name: "color".to_string(),
                        description: "The color to fade".to_string(),
                        help: "".to_string(),
                        value: None,
                        required: true,
                    },
                    MethodArgument {
                        name: "fade_duration".to_string(),
                        description: "Sets the duration per fade cycle".to_string(),
                        help: "".to_string(),
                        value: None,
                        required: true,
                    },
                ],
            },
            Box::new(FunctionFade::new(render_manager.clone())),
        );
    }
}

fn main() -> Result<(), AppError> {
    let mut app = Application::new(Box::new(Ws281xApplication::new()));
    app.boot()?;
    app.multi_run(4)?;

    Ok(())
}
