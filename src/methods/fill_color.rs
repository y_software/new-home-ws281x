use std::sync::{Arc, RwLock};

use new_home_application::method::method_callable::MethodCallable;
use new_home_application::method::method_structure::{MethodArguments, MethodResult};
use new_home_application::method::response_formatter::JsonResponseFormatter;

use crate::services::render_manager::RenderManager;
use crate::shader::static_fill::StaticFill;
use crate::util::color_parser::ColorParser;

pub struct FillColor {
    render_manager: Arc<RwLock<RenderManager>>,
}

impl FillColor {
    pub fn new(render_manager: Arc<RwLock<RenderManager>>) -> Self {
        Self { render_manager }
    }
}

impl MethodCallable for FillColor {
    fn call(&self, arguments: MethodArguments) -> MethodResult {
        let color = match arguments.get_argument(&String::from("color")) {
            Some(color) => match color.as_str() {
                Some(color) => String::from(color),
                None => return JsonResponseFormatter::error(&String::from("Argument 'color' has wrong type"))
            },
            None => return JsonResponseFormatter::error(&String::from("Missing argument 'color'"))
        };
        let channel = match arguments.get_argument(&String::from("channel")) {
            Some(channel) => match channel.as_str() {
                Some(channel) => String::from(channel),
                None => return JsonResponseFormatter::error(&String::from("Argument 'channel' has wrong type"))
            },
            None => return JsonResponseFormatter::error(&String::from("Missing argument 'channel'"))
        };

        let parser = match ColorParser::new(color) {
            Err(error) => return JsonResponseFormatter::error(&format!("{}", error)),
            Ok(color) => color
        };

        match self.render_manager.write().unwrap().set_shader_for(channel, Some(Box::new(StaticFill::new(parser.ws_rgb())))) {
            Ok(_) => {}
            Err(error) => {
                return JsonResponseFormatter::error(&format!("{}", error));
            }
        }

        MethodResult { code: 0, message: None }
    }
}