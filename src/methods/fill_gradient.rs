use std::sync::{Arc, RwLock};

use new_home_application::method::method_callable::MethodCallable;
use new_home_application::method::method_structure::{MethodArguments, MethodResult};
use new_home_application::method::response_formatter::JsonResponseFormatter;
use palette::LinSrgb;

use crate::services::render_manager::RenderManager;
use crate::shader::gradient_fill::GradientFill;
use crate::util::color_parser::ColorParser;

pub struct FillGradient {
    render_manager: Arc<RwLock<RenderManager>>,
}

impl FillGradient {
    pub fn new(render_manager: Arc<RwLock<RenderManager>>) -> Self {
        Self { render_manager }
    }
}

impl MethodCallable for FillGradient {
    fn call(&self, arguments: MethodArguments) -> MethodResult {
        let gradient_arg = arguments.get_argument(&String::from("gradient")).unwrap();
        let gradient = match gradient_arg.as_array() {
            Some(array) => array,
            None => return JsonResponseFormatter::error(&String::from("Argument gradient is not an array!"))
        };

        let gradient_steps: Vec<LinSrgb> = gradient.iter()
            .filter(|value| { value.is_string() })
            .map(|value| { ColorParser::new(String::from(value.as_str().unwrap())) })
            .filter(|value| { value.is_ok() })
            .map(|value| { LinSrgb::from(value.unwrap().hsl()) })
            .collect();

        let channel_arg = arguments.get_argument(&String::from("channel")).unwrap();
        let channel = match channel_arg.as_str() {
            None => return JsonResponseFormatter::error(&String::from("'channel' is not a string")),
            Some(channel) => String::from(channel)
        };

        match self.render_manager.write().unwrap().set_shader_for(channel, Some(Box::new(GradientFill::new(gradient_steps)))) {
            Err(error) => return JsonResponseFormatter::error(&format!("{}", error)),
            Ok(_) => {}
        }

        MethodResult { code: 0, message: None }
    }
}
