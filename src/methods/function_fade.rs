use std::sync::{Arc, RwLock};

use new_home_application::method::method_callable::MethodCallable;
use new_home_application::method::method_structure::{MethodArguments, MethodResult};
use new_home_application::method::response_formatter::JsonResponseFormatter;

use crate::services::render_manager::RenderManager;
use crate::shader::static_fade::StaticFade;
use crate::util::color_parser::ColorParser;

pub struct FunctionFade {
    render_manager: Arc<RwLock<RenderManager>>,
}

impl FunctionFade {
    pub fn new(render_manager: Arc<RwLock<RenderManager>>) -> Self {
        Self { render_manager }
    }
}

impl MethodCallable for FunctionFade {
    fn call(&self, arguments: MethodArguments) -> MethodResult {
        let color_arg = arguments.get_argument(&String::from("color")).unwrap();
        let color = match color_arg.as_str() {
            None => return JsonResponseFormatter::error(&String::from("'color' is not a string")),
            Some(color) => String::from(color)
        };

        let channel_arg = arguments.get_argument(&String::from("channel")).unwrap();
        let channel = match channel_arg.as_str() {
            None => return JsonResponseFormatter::error(&String::from("'channel' is not a string")),
            Some(channel) => String::from(channel)
        };

        let fade_duration_arg = arguments.get_argument(&String::from("fade_duration")).unwrap();
        let fade_duration = match fade_duration_arg.as_f64() {
            None => return JsonResponseFormatter::error(&String::from("'fade_duration' is not a number")),
            Some(fade_duration) => fade_duration as f32
        };

        let parser = match ColorParser::new(color) {
            Err(error) => return JsonResponseFormatter::error(&format!("{}", error)),
            Ok(color) => color
        };

        match self.render_manager.write().unwrap().set_shader_for(channel, Some(Box::new(StaticFade::new(parser.ws_rgb(), fade_duration)))) {
            Ok(_) => {}
            Err(error) => {
                return JsonResponseFormatter::error(&format!("{}", error));
            }
        }

        MethodResult { code: 0, message: None }
    }
}