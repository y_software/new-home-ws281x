use std::fs::read_to_string;

use new_home_application::method::method_callable::MethodCallable;
use new_home_application::method::method_structure::{MethodArguments, MethodResult};
use new_home_application::method::response_formatter::JsonResponseFormatter;

pub struct GetScript {}

impl GetScript {
    pub fn new() -> Self {
        Self {}
    }

    fn get_file_content(&self, path: String) -> String {
        match read_to_string(&path) {
            Err(_) => format!(r#"console.log("Could not read file {}")"#, &path),
            Ok(content) => content
        }
    }
}

impl MethodCallable for GetScript {
    fn call(&self, arguments: MethodArguments) -> MethodResult {
        let script_name = match arguments.get_argument(&"script_name".to_string()) {
            None => {
                return JsonResponseFormatter::error(
                    &"Argument is 'script_name' not set".to_string(),
                );
            }
            Some(value) => match value.as_str() {
                None => {
                    return JsonResponseFormatter::error(
                        &"Argument 'script_name' is not of type string".to_string(),
                    );
                }
                Some(value) => String::from(value.trim_matches('/')),
            },
        };

        if script_name.contains("..") {
            return JsonResponseFormatter::error(&"String path contains illegal characters".to_string());
        }

        let cwd = std::env::current_dir().unwrap().into_os_string().into_string().unwrap();

        MethodResult {
            code: 0,
            message: Some(json!({ "script": self.get_file_content(format!("{}/resources/{}", &cwd, &script_name)) })),
        }
    }
}
