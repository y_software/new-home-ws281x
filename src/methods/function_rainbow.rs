use std::sync::{Arc, RwLock};

use new_home_application::method::method_callable::MethodCallable;
use new_home_application::method::method_structure::{MethodArguments, MethodResult};
use new_home_application::method::response_formatter::JsonResponseFormatter;

use crate::services::render_manager::RenderManager;
use crate::shader::rainbow::Rainbow;

pub struct FunctionRainbow {
    render_manager: Arc<RwLock<RenderManager>>,

}

impl FunctionRainbow {
    pub fn new(render_manager: Arc<RwLock<RenderManager>>) -> Self {
        Self { render_manager }
    }
}

impl MethodCallable for FunctionRainbow {
    fn call(&self, arguments: MethodArguments) -> MethodResult {
        let channel_arg = arguments.get_argument(&String::from("channel")).unwrap();
        let channel = match channel_arg.as_str() {
            None => return JsonResponseFormatter::error(&String::from("'channel' is not a string")),
            Some(channel) => String::from(channel)
        };
        let speed_arg = arguments.get_argument(&String::from("speed")).unwrap();
        let speed = match speed_arg.as_f64() {
            None => return JsonResponseFormatter::error(&String::from("'speed' is not a number")),
            Some(speed) => speed as f32
        };

        let offset_arg = arguments.get_argument(&String::from("offset")).unwrap();
        let offset = match offset_arg.as_f64() {
            None => return JsonResponseFormatter::error(&String::from("'offset' is not a number")),
            Some(offset) => offset as f32
        };

        match self.render_manager.write().unwrap().set_shader_for(channel, Some(Box::new(Rainbow::new(speed, offset)))) {
            Err(error) => return JsonResponseFormatter::error(&format!("{}", error)),
            Ok(_) => {}
        }

        MethodResult {
            code: 0,
            message: None,
        }
    }
}
