use serde_json::Value;
use new_home_application::method::method_callable::MethodCallable;
use new_home_application::method::method_structure::{MethodArguments, MethodResult};

/// Responses with a pre defined action message
pub struct ActionMessage {
    value: Value,
}

impl ActionMessage {
    pub fn new(value: Value) -> Self {
        Self {
            value
        }
    }
}

impl MethodCallable for ActionMessage {
    fn call(&self, _arguments: MethodArguments) -> MethodResult {
        MethodResult {
            code: 0,
            message: Some(self.value.clone())
        }
    }
}
